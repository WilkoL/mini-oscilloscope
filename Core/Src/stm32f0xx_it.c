//#define BUFFERSIZE 256

#include "main.h"
#include "stm32f0xx_it.h"

extern uint8_t token;
extern uint8_t time_base;
extern uint8_t offset;
extern uint8_t trigger;
extern uint16_t colour_time_base;
extern uint16_t colour_offset;
extern uint16_t colour_trigger_level;
extern uint8_t start_trigger_level_line;

uint16_t timeout = 0;
uint8_t menu = 0;

void NMI_Handler(void)
{

}


void HardFault_Handler(void)
{
	while (1)
	{

	}
}


void SVC_Handler(void)
{

}


void PendSV_Handler(void)
{

}


void SysTick_Handler(void)
{

}


void DMA1_Channel1_IRQHandler(void)					//transfer ADC data via DMA is ready
{
	if (LL_DMA_IsActiveFlag_TC1(DMA1))
	{
		LL_DMA_ClearFlag_TC1(DMA1);
		token = 1;
	}
}


void TIM16_IRQHandler(void)									//1 kHz timeout/debounce
{
	static uint8_t rotary_button;
	static uint8_t rotary_pin1;
	static uint8_t rotary_pin2;

	uint8_t step_up;
	uint8_t step_down;

	if (LL_TIM_IsActiveFlag_UPDATE(TIM16))
	{
		while (LL_TIM_IsActiveFlag_UPDATE(TIM16)) LL_TIM_ClearFlag_UPDATE(TIM16);

		rotary_button <<= 1;
		rotary_pin1 <<= 1;
		rotary_pin2 <<= 1;

		if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_2) == 0) rotary_button++;
		if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_9) == 0) rotary_pin1++;
		if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_10) == 0) rotary_pin2++;

		if (rotary_button == 0x7F)
		{
			if (menu < 2) menu++;
			else menu = 0;
		}

		step_up = 0;
		step_down = 0;
		if (rotary_pin1 == 0x7F)
		{
			if (rotary_pin2 == 0x00) step_down = 1;
			if (rotary_pin2 == 0xFF) step_up = 1;
		}

		switch (menu)
		{
		case 0:												//timebase
			colour_time_base = COLOR565_YELLOW;
			colour_offset = COLOR565_WHITE_SMOKE;
			colour_trigger_level = COLOR565_RED;
			start_trigger_level_line = 148;

			if (step_up)
			{
				if (time_base < 12) time_base++;
			}
			if (step_down)
			{
				if (time_base > 0) time_base--;
			}
			break;

		case 1:												//offset
			colour_time_base = COLOR565_WHITE_SMOKE;
			colour_offset = COLOR565_YELLOW;
			colour_trigger_level = COLOR565_RED;

			if (step_up)
			{
				if (offset < 6) offset++;
			}
			if (step_down)
			{
				if (offset > 0) offset--;
			}
			break;

		case 2:												//trigger level
			colour_time_base = COLOR565_WHITE_SMOKE;
			colour_offset = COLOR565_WHITE_SMOKE;
			colour_trigger_level = COLOR565_YELLOW;

			if (step_up)
			{
				if (trigger < 14) trigger++;
			}
			if (step_down)
			{
				if (trigger > 0) trigger--;
			}
			break;

		default:
			break;
		}


		switch(time_base)
		{
		case 0:
			LL_TIM_SetPrescaler(TIM3, 9);
			LL_TIM_SetAutoReload(TIM3, 39999);				//160 Hz		1s/scr		200ms/div
			break;
		case 1:
			LL_TIM_SetPrescaler(TIM3, 9);
			LL_TIM_SetAutoReload(TIM3, 19999);				//320 Hz		500ms		100ms/div
			break;
		case 2:
			LL_TIM_SetPrescaler(TIM3, 9);
			LL_TIM_SetAutoReload(TIM3, 9999);				//640 Hz		250ms		50ms/div
			break;
		case 3:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 39999);				//1.6 kHz		100ms/scr	20ms/div
			break;
		case 4:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 19999);				//3.2 kHz		50ms		10ms/div
			break;
		case 5:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 9999);				//6.4 kHz		25ms		5ms/div
			break;
		case 6:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 3999);				//16 kHz		10ms		2ms/div
			break;
		case 7:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 1999);				//32 kHz		5ms			1ms/div
			break;
		case 8:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 999);				//64 kHz		2.5ms		500us/div
			break;
		case 9:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 399);				//160 kHz		1ms			200us/div
			break;
		case 10:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 199);				//320 kHz		500us		100us/div
			break;
		case 11:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 99);					//640 kHz		250us		50us/div
			break;
		case 12:
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 79);					//800 kHz		200us		40us/div
			break;

		default:											//will never happen
			LL_TIM_SetPrescaler(TIM3, 0);
			LL_TIM_SetAutoReload(TIM3, 1999);
			break;
		}



		switch(offset)
		{
		case 0:
			LL_TIM_OC_SetCompareCH1(TIM14, 1);
			break;
		case 1:
			LL_TIM_OC_SetCompareCH1(TIM14, 99);
			break;
		case 2:
			LL_TIM_OC_SetCompareCH1(TIM14, 199);
			break;
		case 3:
			LL_TIM_OC_SetCompareCH1(TIM14, 299);
			break;
		case 4:
			LL_TIM_OC_SetCompareCH1(TIM14, 399);
			break;
		case 5:
			LL_TIM_OC_SetCompareCH1(TIM14, 499);
			break;
		case 6:
			LL_TIM_OC_SetCompareCH1(TIM14, 598);
			break;

		default:											//will never happen
			LL_TIM_OC_SetCompareCH1(TIM14, 299);
			break;
		}


		if (timeout > 0) timeout--;
		else
		{
			LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_2);
			LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_9);
			LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_10);

			LL_TIM_DisableIT_UPDATE(TIM16);					//timeout en debouncing timer OFF
			LL_TIM_DisableCounter(TIM16);
		}
	}
}


void EXTI2_3_IRQHandler(void)								//rotary encoder BUTTON
{
	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_2) != RESET)
	{
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_2)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_2);
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_2);

		LL_TIM_EnableIT_UPDATE(TIM16);						//timeout and debouncing timer ON
		LL_TIM_EnableCounter(TIM16);

		timeout = 20;										//20ms
	}
}


void EXTI4_15_IRQHandler(void)								//rotary encoder A B
{
	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_9) != RESET)
	{
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_9)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_9);
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_9);

		LL_TIM_EnableIT_UPDATE(TIM16);						//timeout and debouncing timer ON
		LL_TIM_EnableCounter(TIM16);

		timeout = 20;										//20ms
	}

	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_10) != RESET)
	{
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_10)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_10);
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_10);

		LL_TIM_EnableIT_UPDATE(TIM16);						//timeout and debouncing timer ON
		LL_TIM_EnableCounter(TIM16);

		timeout = 200;										//20ms
	}
}
