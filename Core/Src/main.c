#define BUFFERSIZE 320		//twice the number of TFT pixels

#define X_SIZE 160
#define Y_SIZE 128

#define X_MAX 158
#define Y_MAX 127


#include "main.h"

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM14_Init(void);
static void MX_TIM16_Init(void);

uint8_t adc_buffer[BUFFERSIZE];
uint8_t display_buffer[BUFFERSIZE];
uint8_t erase_buffer[BUFFERSIZE];

uint8_t token;
uint8_t time_base;
uint8_t offset;
uint8_t trigger;

uint16_t colour_time_base;
uint16_t colour_offset;
uint16_t colour_trigger_level;
uint8_t start_trigger_level_line;


int main(void)
{
	uint16_t x;

	uint16_t x1;
	uint16_t y1;

	uint16_t x2;
	uint16_t y2;

	uint16_t x3;
	uint16_t y3;

	uint16_t x4;
	uint16_t y4;

	uint8_t trigger_level;
	uint16_t trigger_point;
	uint16_t previous_trigger_point;


	LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SYSCFG);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_SPI1_Init();
	//MX_TIM1_Init();
	MX_TIM3_Init();
	MX_TIM14_Init();
	MX_TIM16_Init();

	LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t) &ADC1->DR);
	LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t) &adc_buffer);
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, BUFFERSIZE);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);					//Enable IRQ bij transfer complete
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);				//Enable de DMA transfer


	//calibrate BEFORE the adc-conversions are started by the timer interrupt
	//the DMA transfers are temporarily disabled to prevent the calibration
	//results from entering into the DMA stream

	LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_NONE);
	ADC1->CR = (uint32_t) 0;									//do NOT use LL_ADC_Disable(ADC1);
	LL_ADC_StartCalibration(ADC1);								//it wil set ADDIS but will NOT disable the ADC
	while (LL_ADC_IsCalibrationOnGoing(ADC1));
	LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);
	LL_mDelay(10);
	LL_ADC_Enable(ADC1);
	LL_mDelay(10);

	LL_SPI_Enable(SPI1);
	ST7735_Init();
	ST7735_AddrSet(0,0,X_SIZE,Y_SIZE);
	ST7735_Clear(COLOR565_BLACK);
	ST7735_Orientation(scr_CW);

	LL_TIM_EnableCounter(TIM14);								//pwm for offset

	LL_ADC_REG_StartConversion(ADC1);
	LL_TIM_EnableCounter(TIM3);									//triggering ADC (timebase)

	LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_2);						//rotary encoder button
	LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_9);						//rotary encoder A (B)



	time_base = 7;
	LL_TIM_SetPrescaler(TIM3, 0);
	LL_TIM_SetAutoReload(TIM3, 1999);							//1ms/div
	colour_time_base = COLOR565_YELLOW;

	offset = 3;
	LL_TIM_OC_SetCompareCH1(TIM14, 299);						//offset 0V
	colour_offset = COLOR565_WHITE_SMOKE;

	trigger = 7;												//position 63, middle of screen
	colour_trigger_level = COLOR565_WHITE_SMOKE;
	start_trigger_level_line = 148;


	trigger_level = 63;
	trigger_point = 1;
	previous_trigger_point = 1;
	token = 1;

	while (1)
	{
		if (token == 1)											//new data is available
		{
			LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_1);

			for (x = 0; x < (BUFFERSIZE); x++)					//copy data from adc_buffer to display_buffer
			{													//the adc is continuously converting
				display_buffer[x] = adc_buffer[x];
			}

			trigger_point = 1;
			for (x = 1; x < (BUFFERSIZE - X_MAX); x++)			//find trigger level
			{
				if ((display_buffer[x] >= trigger_level) && (display_buffer[x + 1] <= trigger_level))
				{
					trigger_point = x;
					break;
				}
			}
			if (trigger_point > 600) trigger_point = 1;			//trigger level "not found"


			//delete all "old" lines
			ST7735_HLine(2, 158, 119, COLOR565_BLACK);
			ST7735_HLine(2, 158, 111, COLOR565_BLACK);
			ST7735_HLine(2, 158, 103, COLOR565_BLACK);
			ST7735_HLine(2, 158, 95, COLOR565_BLACK);
			ST7735_HLine(2, 158, 87, COLOR565_BLACK);
			ST7735_HLine(2, 158, 79, COLOR565_BLACK);
			ST7735_HLine(2, 158, 71, COLOR565_BLACK);
			ST7735_HLine(2, 158, 62, COLOR565_BLACK);
			ST7735_HLine(2, 158, 55, COLOR565_BLACK);
			ST7735_HLine(2, 158, 47, COLOR565_BLACK);
			ST7735_HLine(2, 158, 39, COLOR565_BLACK);
			ST7735_HLine(2, 158, 31, COLOR565_BLACK);
			ST7735_HLine(2, 158, 23, COLOR565_BLACK);
			ST7735_HLine(2, 158, 15, COLOR565_BLACK);
			ST7735_HLine(2, 158, 7, COLOR565_BLACK);

			//show raster
			ST7735_HLine(0, 159, 0, COLOR565_GRAY);
			ST7735_HLine(0, 159, 21, COLOR565_GRAY);
			ST7735_HLine(0, 159, 42, COLOR565_GRAY);
			ST7735_HLine(0, 159, 63, COLOR565_GRAY);
			ST7735_HLine(0, 159, 84, COLOR565_GRAY);
			ST7735_HLine(0, 159, 105, COLOR565_GRAY);
			ST7735_HLine(0, 159, 127, COLOR565_GRAY);

			ST7735_VLine(0, 0, 127, COLOR565_GRAY);
			ST7735_VLine(31, 0, 127, COLOR565_GRAY);
			ST7735_VLine(63, 0, 127, COLOR565_GRAY);
			ST7735_VLine(95, 0, 127, COLOR565_GRAY);
			ST7735_VLine(127, 0, 127, COLOR565_GRAY);
			ST7735_VLine(159, 0, 127, COLOR565_GRAY);

			//show timebase value
			switch (time_base)
			{
			case 0:
				ST7735_PutStr5x7(1, 100, 115,"200ms/div ", colour_time_base, COLOR565_BLACK);
				break;
			case 1:
				ST7735_PutStr5x7(1, 100, 115,"100ms/div ", colour_time_base, COLOR565_BLACK);
				break;
			case 2:
				ST7735_PutStr5x7(1, 100, 115,"50ms/div  ", colour_time_base, COLOR565_BLACK);
				break;
			case 3:
				ST7735_PutStr5x7(1, 100, 115,"20ms/div ", colour_time_base, COLOR565_BLACK);
				break;
			case 4:
				ST7735_PutStr5x7(1, 100, 115,"10ms/div ", colour_time_base, COLOR565_BLACK);
				break;
			case 5:
				ST7735_PutStr5x7(1, 100, 115,"5ms/div  ", colour_time_base, COLOR565_BLACK);
				break;
			case 6:
				ST7735_PutStr5x7(1, 100, 115,"2ms/div  ", colour_time_base, COLOR565_BLACK);
				break;
			case 7:
				ST7735_PutStr5x7(1, 100, 115,"1ms/div  ", colour_time_base, COLOR565_BLACK);
				break;
			case 8:
				ST7735_PutStr5x7(1, 100, 115,"500us/div", colour_time_base, COLOR565_BLACK);
				break;
			case 9:
				ST7735_PutStr5x7(1, 100, 115,"200us/div", colour_time_base, COLOR565_BLACK);
				break;
			case 10:
				ST7735_PutStr5x7(1, 100, 115,"100us/div", colour_time_base, COLOR565_BLACK);
				break;
			case 11:
				ST7735_PutStr5x7(1, 100, 115,"50us/div ", colour_time_base, COLOR565_BLACK);
				break;
			case 12:
				ST7735_PutStr5x7(1, 100, 115,"40us/div ", colour_time_base, COLOR565_BLACK);
				break;

			default:
				ST7735_PutStr5x7(1, 100, 115,"5ms/div  ", colour_time_base, COLOR565_BLACK);
				break;
			}


			//sensitivity is always 1V/div
			ST7735_PutStr5x7(1, 10, 115,"1V/div", COLOR565_WHITE_SMOKE, COLOR565_BLACK);


			//show zero volt line
			switch (offset)
			{
			case 0:
				ST7735_HLine(0, 159, 127, colour_offset);
				break;
			case 1:
				ST7735_HLine(0, 159, 105, colour_offset);
				break;
			case 2:
				ST7735_HLine(0, 159, 84, colour_offset);
				break;
			case 3:
				ST7735_HLine(0, 159, 63, colour_offset);
				break;
			case 4:
				ST7735_HLine(0, 159, 42, colour_offset);
				break;
			case 5:
				ST7735_HLine(0, 159, 21, colour_offset);
				break;
			case 6:
				ST7735_HLine(0, 159, 0, colour_offset);
				break;

			default:
				ST7735_HLine(0, 159, 63, colour_offset);
				break;
			}



			//show trigger level
			switch (trigger)
			{
			case 0:
				ST7735_HLine(start_trigger_level_line, 158, 119, colour_trigger_level);
				trigger_level = 238;
				break;
			case 1:
				ST7735_HLine(start_trigger_level_line, 158, 111, colour_trigger_level);
				trigger_level = 222;
				break;
			case 2:
				ST7735_HLine(start_trigger_level_line, 158, 103, colour_trigger_level);
				trigger_level = 206;
				break;
			case 3:
				ST7735_HLine(start_trigger_level_line, 158, 95, colour_trigger_level);
				trigger_level = 190;
				break;
			case 4:
				ST7735_HLine(start_trigger_level_line, 158, 87, colour_trigger_level);
				trigger_level = 174;
				break;
			case 5:
				ST7735_HLine(start_trigger_level_line, 158, 79, colour_trigger_level);
				trigger_level = 158;
				break;
			case 6:
				ST7735_HLine(start_trigger_level_line, 158, 71, colour_trigger_level);
				trigger_level = 142;
				break;
			case 7:
				ST7735_HLine(start_trigger_level_line, 158, 62, colour_trigger_level);
				trigger_level = 126;
				break;
			case 8:
				ST7735_HLine(start_trigger_level_line, 158, 55, colour_trigger_level);
				trigger_level = 110;
				break;
			case 9:
				ST7735_HLine(start_trigger_level_line, 158, 47, colour_trigger_level);
				trigger_level = 94;
				break;
			case 10:
				ST7735_HLine(start_trigger_level_line, 158, 39, colour_trigger_level);
				trigger_level = 78;
				break;
			case 11:
				ST7735_HLine(start_trigger_level_line, 158, 31, colour_trigger_level);
				trigger_level = 62;
				break;
			case 12:
				ST7735_HLine(start_trigger_level_line, 158, 23, colour_trigger_level);
				trigger_level = 46;
				break;
			case 13:
				ST7735_HLine(start_trigger_level_line, 158, 15, colour_trigger_level);
				trigger_level = 30;
				break;
			case 14:
				ST7735_HLine(start_trigger_level_line, 158, 7, colour_trigger_level);
				trigger_level = 14;
				break;

			default:
				ST7735_HLine(start_trigger_level_line, 158, 63, colour_trigger_level);
				trigger_level = 126;
				break;
			}



			//erase old trace and show the current trace
			for (x = 1; x < X_MAX; x++)
			{

				x1 = x + previous_trigger_point;
				x2 = x1 + 1;
				x3 = x + trigger_point;
				x4 = x3 + 1;

				y1 = erase_buffer[x1] >> 1;
				y2 = erase_buffer[x2] >> 1;
				y3 = display_buffer[x3] >> 1;
				y4 = display_buffer[x4] >> 1;

				ST7735_Line(x, y1, x+1, y2, COLOR565_BLACK);		//old
				ST7735_Line(x, y3, x+1, y4, COLOR565_GREEN_YELLOW);	//new
			}

			//store values, used next time to erase the old trace
			for (x = 0; x < BUFFERSIZE; x++)
			{
				erase_buffer[x] = display_buffer[x];
			}
			previous_trigger_point = trigger_point;


			LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_1);
			LL_mDelay(0);	//1ms
			token = 0;
		}

	}
}


void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_1);
	if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_1) Error_Handler();
	LL_FLASH_EnablePrefetch();
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_2, LL_RCC_PLL_MUL_16);
	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(64000000);
	LL_SetSystemCoreClock(64000000);
}



static void MX_ADC_Init(void)
{
	LL_ADC_InitTypeDef ADC_InitStruct = {0};
	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_ADC1);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PRIORITY_HIGH);
	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);
	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_NOINCREMENT);
	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_BYTE);
	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_BYTE);

	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_0);

	ADC_InitStruct.Clock = LL_ADC_CLOCK_SYNC_PCLK_DIV4;
	ADC_InitStruct.Resolution = LL_ADC_RESOLUTION_8B;
	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.LowPowerMode = LL_ADC_LP_MODE_NONE;
	LL_ADC_Init(ADC1, &ADC_InitStruct);

	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_EXT_TIM3_TRGO;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_LIMITED;
	ADC_REG_InitStruct.Overrun = LL_ADC_REG_OVR_DATA_PRESERVED;
	LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);

	LL_ADC_REG_SetSequencerScanDirection(ADC1, LL_ADC_REG_SEQ_SCAN_DIR_FORWARD);
	LL_ADC_SetSamplingTimeCommonChannels(ADC1, LL_ADC_SAMPLINGTIME_1CYCLE_5);
	LL_ADC_DisableIT_EOC(ADC1);
	LL_ADC_DisableIT_EOS(ADC1);
	LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
}


static void MX_SPI1_Init(void)
{
	LL_SPI_InitTypeDef SPI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_SPI1);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_5;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
	SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
	SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
	SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
	SPI_InitStruct.ClockPhase = LL_SPI_PHASE_1EDGE;
	SPI_InitStruct.NSS = LL_SPI_NSS_SOFT;
	SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV4;
	SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
	SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
	SPI_InitStruct.CRCPoly = 7;
	LL_SPI_Init(SPI1, &SPI_InitStruct);

	LL_SPI_SetStandard(SPI1, LL_SPI_PROTOCOL_MOTOROLA);
	LL_SPI_EnableNSSPulseMgt(SPI1);
}


static void MX_TIM3_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 1999;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_EnableARRPreload(TIM3);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM3);
}


static void MX_DMA_Init(void) 
{
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

	NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}


static void MX_TIM14_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};

	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM14);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 599;							//107 kHz
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM14, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM14);
	LL_TIM_OC_EnablePreload(TIM14, LL_TIM_CHANNEL_CH1);
	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM2;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_ENABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 299;						//50% duty
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM14, LL_TIM_CHANNEL_CH1, &TIM_OC_InitStruct);
	LL_TIM_OC_DisableFast(TIM14, LL_TIM_CHANNEL_CH1);
}


static void MX_TIM16_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_TIM16);

	NVIC_SetPriority(TIM16_IRQn, 0);
	NVIC_EnableIRQ(TIM16_IRQn);

	TIM_InitStruct.Prescaler = 63;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 999;							//1 kHz
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM16, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM16);
}


static void MX_GPIO_Init(void)
{
	LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_3);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);
	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_6);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1 | LL_GPIO_PIN_2 | LL_GPIO_PIN_3 | LL_GPIO_PIN_4 | LL_GPIO_PIN_6;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE2);
	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE9);
	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE10);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_2, LL_GPIO_PULL_DOWN);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_DOWN);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_DOWN);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_2, LL_GPIO_MODE_INPUT);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_INPUT);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_INPUT);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_2;
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_9;
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_10;
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	NVIC_SetPriority(EXTI2_3_IRQn, 0);
	NVIC_EnableIRQ(EXTI2_3_IRQn);
	NVIC_SetPriority(EXTI4_15_IRQn, 0);
	NVIC_EnableIRQ(EXTI4_15_IRQn);
}


void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{ 

}
#endif
