# Mini Oscilloscope

A small oscilloscope with a STM32F030F4 and ST7735 (1.8 inch TFT)
The STM32 is slightly overclocked at 64MHz.

Voltage range from -6V up to 6V (good for Arduino)
Timebase from 200ms/div to 40us/div